const {Router} = require('express'),
    router = Router(),
    controller = require('../controllers/controller');

router.route('/')
    .get(controller.getHome)
router.route('/fastFashion')
    .get(controller.getFastFashion)
router.route('/endOfLife')
    .get(controller.getEndOfLife)    
router.route('/politicalInfrastructure')
    .get(controller.getPoliticalInfrastructure)
router.route('/solutionsToWaste')
    .get(controller.getSolutionsToWaste)
router.route('/caseStudy')
    .get(controller.getCaseStudy)
router.route('/sources')
    .get(controller.getSources)
module.exports = router