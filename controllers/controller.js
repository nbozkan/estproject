exports.getHome = async(req, res) => {
    res.status(200).render('home')
}

exports.getFastFashion = async(req, res) => {
    res.status(200).render('fastFashion')
}

exports.getEndOfLife = async(req, res) => {
    res.status(200).render('endOfLife')
}

exports.getPoliticalInfrastructure = async(req, res) => {
    res.status(200).render('politicalInfrastructure')
}

exports.getSolutionsToWaste = async(req, res) => {
    res.status(200).render('solutionsToWaste')
}

exports.getCaseStudy = async(req, res) => {
    res.status(200).render('caseStudy')
}

exports.getSources = async(req,res) => {
    res.status(200).render('sources')
}